package guru.springframework.sfgpetclinic.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "specialties")
public class Speciality extends BaseEntity {

    private static final long serialVersionUID = 1L;
	
    @Column(name = "description")
    private String description;

    public Speciality() {
	}
    
	public Speciality(String description) {
		super();
		this.description = description;
	}

	public Speciality(Long id, String description) {
		super(id);
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

    
    
}
